window.dqdt = window.dqdt || {};
dqdt.init = function () {
	dqdt.showPopup();
	dqdt.hidePopup();	
};
/********************************************************
# Sidebar category
********************************************************/
$('.nav-category .fa-angle-down').click(function(e){
	$(this).parent().toggleClass('active');
});
/********************************************************
# Offcanvas menu
********************************************************/
jQuery(document).ready(function ($) {
	var ofheight = $(document).height();	
	$('.off-canvas-menu').css('height',ofheight);
});
/********************************************************
# Footer
********************************************************/
$('.site-footer h3').click(function() {
	$(this).parent().find('.list-menu').toggleClass('active'); 	
	$(this).toggleClass('active'); 	
}); 
$('.aside.aside-mini-products-list .aside-title h3').click(function() {
	$(this).parents('.aside-mini-products-list').find('#collection-filters-container').toggleClass('active'); 	
}); 

/**/
/********************************************************
# toggle-menu
********************************************************/

$('.toggle-menu .caret').click(function() {
	$(this).closest('li').find('> .sub-menu').slideToggle("fast");
	$(this).closest('li').toggleClass("open");
	return false;              
}); 
$('.off-canvas-toggle').click(function(){
	$('body').toggleClass('show-off-canvas');
	$('.off-canvas-menu').toggleClass('open');
})
$('body').click(function(event) {
	if (!$(event.target).closest('.off-canvas-menu').length) {
		$('body').removeClass('show-off-canvas');
	};
});

/********************************************************
# accordion
********************************************************/
$('.accordion .nav-link').click(function(e){
	e.preventDefault;

	$(this).parent().toggleClass('active');
})
/********************************************************
# Dropdown
********************************************************/
$('.dropdown-toggle').click(function() {
	$(this).parent().toggleClass('open'); 	
}); 
$('#dropdownMenu1').click(function() {
	var ofh= $(this).parent().find('.dropdown-menu').width();

	var mm = $('.menu-mobile'). offset().left;

	$('.site-header-inner button.btn-close').css('left',ofh - mm +'px');
}); 
$('.btn-close').click(function() {
	$(this).parents('.dropdown').toggleClass('open');
}); 
$('body').click(function(event) {
	if (!$(event.target).closest('.dropdown').length) {
		$('.dropdown').removeClass('open');
	};
});

$('body').click(function(event) {
	if (!$(event.target).closest('.collection-selector').length) {
		$('.list_search').css('display','none');
	};
});


$('.tab_top_products .nav-tabs>li').click(function(){
	$('.tab_top_products .product-box .product-thumbnail a img.bethua').css('padding-top',"0");
	setTimeout(function(){
		
		$('.product-box .product-thumbnail a img').each(function(){
			var t1 = (this.naturalHeight/this.naturalWidth);		
			var t2 = ($(this).parent().height()/$(this).parent().width());
			if(t1< t2){
				$(this).addClass('bethua');
			}

			var m1 = $(this).height();
			var m2 = $(this).parent().height();
			if(m1 < m2){
				$(this).css('padding-top',(m2-m1)/2 + 'px');
			}
		});
	},500);
})

/********************************************************
# Tab
********************************************************/
$(".e-tabs").each( function(){
	$(this).find('.tabs-title li:first-child').addClass('current');
	$(this).find('.tab-content').first().addClass('current');

	$(this).find('.tabs-title li').click(function(){

		var tab_id = $(this).attr('data-tab');

		var url = $(this).attr('data-url');
		$(this).closest('.e-tabs').find('.tab-viewall').attr('href',url);

		$(this).closest('.e-tabs').find('.tabs-title li').removeClass('current');
		$(this).closest('.e-tabs').find('.tab-content').removeClass('current');

		$(this).addClass('current');
		$(this).closest('.e-tabs').find("#"+tab_id).addClass('current');
	});    
});




/********************************************************
# SHOW NOITICE
********************************************************/
dqdt.showNoitice = function (selector) {   
	$(selector).animate({right: '0'}, 500);
	setTimeout(function() {
		$(selector).animate({right: '-300px'}, 500);
	}, 3500);
};

/********************************************************
# SHOW LOADING
********************************************************/
dqdt.showLoading = function (selector) {    
	var loading = $('.loader').html();
	$(selector).addClass("loading").append(loading);  
}

/********************************************************
# HIDE LOADING
********************************************************/
dqdt.hideLoading = function (selector) {  
	$(selector).removeClass("loading"); 
	$(selector + ' .loading-icon').remove();
}


/********************************************************
# SHOW POPUP
********************************************************/
dqdt.showPopup = function (selector) {
	$(selector).addClass('active');
};

/********************************************************
# HIDE POPUP
********************************************************/
dqdt.hidePopup = function (selector) {
	$(selector).removeClass('active');
}


/************************************************/
$(document).on('click','.overlay, .close-popup, .btn-continue, .fancybox-close', function() {   
	dqdt.hidePopup('.dqdt-popup'); 
	setTimeout(function(){
		$('.loading').removeClass('loaded-content');
	},500);
	return false;
})

jQuery(document).ready(function ($) {
	
	$('.owl-carousel:not(.not-dqowl)').each( function(){
		var xs_item = $(this).attr('data-xs-items');
		var md_item = $(this).attr('data-md-items');
		var sm_item = $(this).attr('data-sm-items');	
		var margin=$(this).attr('data-margin');
		var dot=$(this).attr('data-dot');
		if (typeof margin !== typeof undefined && margin !== false) {    
		} else{
			margin = 30;
		}
		if (typeof xs_item !== typeof undefined && xs_item !== false) {    
		} else{
			xs_item = 1;
		}
		if (typeof sm_item !== typeof undefined && sm_item !== false) {    

		} else{
			sm_item = 3;
		}	

		if (typeof md_item !== typeof undefined && md_item !== false) {    
		} else{
			md_item = 3;
		}
		if (typeof dot !== typeof undefined && dot !== true) {   
			dot= true;
		} else{
			dot = false;
		}

		$(this).owlCarousel({
			loop:false,
			margin:Number(margin),
			responsiveClass:true,
			dots:dot,
			responsive:{
				0:{
					items:Number(xs_item),
					nav:true
				},
				600:{
					items:Number(sm_item),
					nav:false
				},
				1000:{
					items:Number(md_item),
					nav:true,
					loop:false
				}
			}
		})
	})

	/* Back to top */	

	if ($('.back-to-top').length) {

		var scrollTrigger = 100, // px
			backToTop = function () {
				var scrollTop = $(window).scrollTop();
				if (scrollTop > scrollTrigger) {
					$('.back-to-top').addClass('show');
				} else {
					$('.back-to-top').removeClass('show');
				}
			};
		backToTop();
		$(window).on('scroll', function () {
			backToTop();
		});
		$('.back-to-top').on('click', function (e) {

			e.preventDefault();
			$('html,body').animate({				
				scrollTop: 0
			}, 700);
		});
	}
});

jQuery(document).ready(function ($) {

	$('.product-box .product-thumbnail a img').each(function(){
		var t1 = (this.naturalHeight/this.naturalWidth);		
		var t2 = ($(this).parent().height()/$(this).parent().width());
		if(t1< t2){
			$(this).addClass('bethua');
		}

		var m1 = $(this).height();
		var m2 = $(this).parent().height();
		if(m1 < m2){
			$(this).css('padding-top',(m2-m1)/2 + 'px');
		}
	});


	var $sync1 = $(".product-images"),
		$sync2 = $(".thumbnail-product"),
		flag = false,
		duration = 300;


	$sync1
		.owlCarousel({
		items: 1,
		margin: 30,
		nav: false,
		dots: false,
		navText: [ 
			'<i class="fa fa-chevron-left" aria-hidden="true"></i>', 
			'<i class="fa fa-chevron-right" aria-hidden="true"></i>' 
		],
	})
		.on('changed.owl.carousel', function (e) {
		if (!flag) {
			flag = true;
			$sync2.trigger('to.owl.carousel', [e.item.index, duration, true]);
			flag = false;
		}
	});

	$sync2
		.owlCarousel({
		margin: 12,
		items: 3,
		nav: true,
		dots: false,
	})
		.on('click', '.owl-item', function () {
		$sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);

	})
		.on('changed.owl.carousel', function (e) {
		if (!flag) {
			flag = true;		
			$sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
			flag = false;
		}
	});
});





/********************************************************
# Quickview
********************************************************/
initQuickView();
var product = {};
var currentLinkQuickView = '';
var option1 = '';
var option2 = '';
function setButtonNavQuickview() {
	$("#quickview-nav-button a").hide();
	$("#quickview-nav-button a").attr("data-index", "");
	var listProducts = $(currentLinkQuickView).closest(".slide").find("a.quick-view");
	if(listProducts.length > 0) {
		var currentPosition = 0;
		for(var i = 0; i < listProducts.length; i++) {
			if($(listProducts[i]).data("handle") == $(currentLinkQuickView).data("handle")) {
				currentPosition = i;
				break;
			}
		}
		if(currentPosition < listProducts.length - 1) {
			$("#quickview-nav-button .btn-next-product").show();
			$("#quickview-nav-button .btn-next-product").attr("data-index", currentPosition + 1);
		}
		if(currentPosition > 0) {
			$("#quickview-nav-button .btn-previous-product").show();
			$("#quickview-nav-button .btn-previous-product").attr("data-index", currentPosition - 1);
		}
	}
	$("#quickview-nav-button a").click(function() {
		$("#quickview-nav-button a").hide();
		var indexLink = parseInt($(this).data("index"));
		if(!isNaN(indexLink) && indexLink >= 0) {
			var listProducts = $(currentLinkQuickView).closest(".slide").find("a.quick-view");
			if(listProducts.length > 0 && indexLink < listProducts.length) {
				//$(".quickview-close").trigger("click");
				$(listProducts[indexLink]).trigger("click");
			}
		}
	});
}
function initQuickView(){
	$(document).on("click", "#thumblist_quickview li", function() {		
		changeImageQuickView($(this).find("img:first-child"), ".product-featured-image-quickview");
		$(this).parent().parent().find('li').removeClass('active');
		$(this).addClass('active');
	});
	/*
	$(document).on("click", ".btn-quickview", function(e) {
		e.preventDefault();		
		$("#quick-view-product").hide();
		var form = $(this).parents('form');			
		$.ajax({
			type: 'POST',
			url: '/cart/add.js',
			async: false,
			data: form.serialize(),
			dataType: 'json',
			error: addToCartFail,
			beforeSend: function() {  				
				if(window.theme_load == "icon"){
					dqdt.showLoading('.btn-addToCart');
				} else{
					dqdt.showPopup('.loading');
				}
			},
			success: addToCartSuccess,
			cache: false
		});

	});
	*/

	$(document).on('click', '.quick-view', function(e) {
		e.preventDefault();
		dqdt.showPopup('.loading');
		var producthandle = $(this).data("handle");
		currentLinkQuickView = $(this);
		Bizweb.getProduct(producthandle,function(product) {
			var qvhtml = $("#quickview-modal").html();
			$(".quick-view-product").html(qvhtml);
			var quickview= $(".quick-view-product");
			if(product.content != null){
				var productdes = product.content.replace(/(<([^>]+)>)/ig,"");
			}else{
				var productdes = "";
			}
			var featured_image = product.featured_image;
			if(featured_image == null){
				featured_image = 'http://bizweb.dktcdn.net/thumb/grande/assets/themes_support/noimage.gif';
			}

			// Reset current link quickview and button navigate in Quickview
			setButtonNavQuickview();
			productdes = productdes.split(" ").splice(0,60).join(" ")+"...";	
			if(featured_image != null){
				quickview.find(".view_full_size img").attr("src",featured_image);
			}

			if(product.price < 1 && product.variants.length < 2){				
				quickview.find(".price").html('Liên hệ');
				quickview.find("del").html('');
				quickview.find("#quick-view-product form").hide();
				quickview.find(".prices").html('<span class="price h2">Liên hệ</span>');
				quickview.find(".add_to_cart_detail span").html('Liên hệ');
			}
			else{
				quickview.find("#quick-view-product form").show();
				quickview.find(".price").html(Bizweb.formatMoney(product.price, "{{amount_no_decimals_with_comma_separator}}₫" ));
			}

			quickview.find(".product-item").attr("id", "product-" + product.id);
			quickview.find(".qv-link").attr("href",product.url);
			quickview.find(".variants").attr("id", "product-actions-" + product.id);
			quickview.find(".variants select").attr("id", "product-select-" + product.id);

			quickview.find(".qwp-name").text(product.name);
			quickview.find(".review .shopify-product-reviews-badge").attr("data-id",product.id);
			if(product.vendor){
				quickview.find(".brand").append("<b>Hãng: </b>"+product.vendor);
			}else{
				quickview.find(".brand").append("<b>Hãng: </b>Không có");
			}
			if(product.product_type){
				quickview.find(".type").append("<b>Loại: </b>"+product.product_type);
			}else{
				quickview.find(".type").append("<b>Loại: </b>Không có");
			}
			if(product.available){
				quickview.find(".availability").html("<b> Tình trạng: </b>Còn hàng");
			}else{                            
				quickview.find(".availability").html("<b> Tình trạng: </b>Hết hàng");
			}
			if(product.variants[0].sku){
				quickview.find(".product-sku").append("<b>Mã sản phẩm: </b>"+product.variants[0].sku);
			}else{
				quickview.find(".product-sku").append("<b>Mã sản phẩm: </b>Không có");
			}
			quickview.find(".product-description").html(productdes);

			if (product.compare_at_price_max > product.price) {

				quickview.find(".old-price").html(Bizweb.formatMoney(product.compare_at_price_max, "{{amount_no_decimals_with_comma_separator}}₫" )).show();
				quickview.find(".price").addClass("sale-price")
			}
			else {
				quickview.find(".old-price").html("");
				quickview.find(".price").removeClass("sale-price")
			}
			if (!product.available) {


				quickViewVariantsSwatch(product, quickview);

				quickview.find(".add_to_cart_detail").text("Hết hàng").addClass("disabled").attr("disabled", "disabled");				
				if(product.variants.length > 1){

					quickview.find("select, .dec, .inc").show();

				}else{
					quickview.find("select, .dec, .inc").hide();
				}

			}
			else {
				quickViewVariantsSwatch(product, quickview);
				if(product.variants.length > 1){
					$('#quick-view-product form').show();

				}else{
					if(product.price < 1){

						$('#quick-view-product form').hide();
					}else{
						$('#quick-view-product form').show();
					}
				}



			}
			quickview.find('.more_info_block .page-product-heading li:first, .more_info_block .tab-content section:first').addClass('active');

			$("#quick-view-product").fadeIn(500);

			$(".view_scroll_spacer").removeClass("hidden");

			loadQuickViewSlider(product, quickview);

			//initQuickviewAddToCart();

			$(".quick-view").fadeIn(500);
			if ($(".quick-view .total-price").length > 0) {
				$(".quick-view input[name=quantity]").on("change", updatePricingQuickView)
			}			
			updatePricingQuickView();
			// Setup listeners to add/subtract from the input
			$(".js-qty__adjust").on("click", function() {
				var el = $(this),
					id = el.data("id"),
					qtySelector = el.siblings(".js-qty__num"),
					qty = parseInt(qtySelector.val().replace(/\D/g, ''));

				var qty = validateQty(qty);

				// Add or subtract from the current quantity
				if (el.hasClass("js-qty__adjust--plus")) {
					qty = qty + 1;
				} else {
					qty = qty - 1;
					if (qty <= 1) qty = 1;
				}

				// Update the input's number
				qtySelector.val(qty);
				updatePricingQuickView();
			});
			$(".js-qty__num").on("change", function() {
				updatePricingQuickView();
			});
		});
		return false;
	});
}

function loadQuickViewSlider(n, r) {
	productImage();
	var loadingImgQuickView = $('.loading-imgquickview');
	var s = Bizweb.resizeImage(n.featured_image, "grande");
	r.find(".quickview-featured-image").append('<a href="' + n.url + '"><img src="' + s + '" title="' + n.title + '"/><div style="height: 100%; width: 100%; top:0; left:0 z-index: 2000; position: absolute; display: none; background: url(' + window.loading_url + ') 50% 50% no-repeat;"></div></a>');
	if (n.images.length > 0) {
		var o = r.find(".more-view-wrapper ul .swiper-wrapper");
		for (i in n.images) {
			var u = Bizweb.resizeImage(n.images[i], "grande");
			var a = Bizweb.resizeImage(n.images[i], "compact");
			var f = '<li class="swiper-slide"><a href="javascript:void(0)" data-imageid="' + n.id + '"" data-zoom-image="' + u + '" ><img src="' + u + '" alt="Proimage" /></a></li>';
			o.append(f)
		}
		o.find("a").click(function() {
			var t = r.find("#product-featured-image-quickview");
			if (t.attr("src") != $(this).attr("data-image")) {
				t.attr("src", $(this).attr("data-image"));
				loadingImgQuickView.show();
				t.load(function(t) {
					loadingImgQuickView.hide();
					$(this).unbind("load");
					loadingImgQuickView.hide()
				})
			}
		});
		var swipers = new Swiper('.swiper-quickview', {
			pagination: '.swiper-pagination',
			paginationClickable: true,
			direction: 'vertical',
			slidesPerView: $(this).data('items'),
			spaceBetween: $(this).data('margin'),
			paginationClickable: true,
			grabCursor: true
		});
		
	} else {        
		r.find(".quickview-more-views").remove();
		r.find(".quickview-more-view-wrapper-jcarousel").remove()
	}
}
function quickViewVariantsSwatch(t, quickview) {

	var v = '<input type="hidden" name="id" value="' + t.id + '">';
	quickview.find("form.variants").append(v);
	if (t.variants.length > 1) {	
		for (var r = 0; r < t.variants.length; r++) {
			var i = t.variants[r];
			var s = '<option value="' + i.id + '">' + i.title + "</option>";
			quickview.find("form.variants > select").append(s)
		}
		var ps = "product-select-" + t.id;
		new Bizweb.OptionSelectors( ps, { 
			product: t, 
			onVariantSelected: selectCallbackQuickView
		});
		if (t.options.length == 1) {

			quickview.find(".selector-wrapper:eq(0)").prepend("<label>" + t.options[0].name + "</label>")

		}
		quickview.find("form.variants .selector-wrapper label").each(function(n, r) {
			$(this).html(t.options[n].name)
		})
	}
	else {
		quickview.find("form.variants > select").remove();
		var q = '<input type="hidden" name="variantId" value="' + t.variants[0].id + '">';
		quickview.find("form.variants").append(q);
	}
}
function productImage() {
	$('#thumblist').owlCarousel({
		navigation: true,
		items: 4,
		itemsDesktop: [1199, 4],
		itemsDesktopSmall: [979, 4],
		itemsTablet: [768, 4],
		itemsTabletSmall: [540, 4],
		itemsMobile: [360, 4]
	});

	if (!!$.prototype.fancybox){
		$('li:visible .fancybox, .fancybox.shown').fancybox({
			'hideOnContentClick': true,
			'openEffect'	: 'elastic',
			'closeEffect'	: 'elastic'
		});
	}
}
/* Quick View ADD TO CART */

function updatePricingQuickView() {

	//Currency.convertAll(window.shop_currency, $("#currencies a.selected").data("currency"), "span.money", "money_format")

	var regex = /([0-9]+[.|,][0-9]+[.|,][0-9]+)/g;
	var unitPriceTextMatch = jQuery('.quick-view-product .price').text().match(regex);
	if (!unitPriceTextMatch) {
		regex = /([0-9]+[.|,][0-9]+)/g;
		unitPriceTextMatch = jQuery('.quick-view-product .price').text().match(regex);
	}
	if (unitPriceTextMatch) {
		var unitPriceText = unitPriceTextMatch[0];
		var unitPrice = unitPriceText.replace(/[.|,]/g,'');
		var quantity = parseInt(jQuery('.quick-view-product input[name=quantity]').val());
		var totalPrice = unitPrice * quantity;
		var totalPriceText = Bizweb.formatMoney(totalPrice, "{{amount_no_decimals_with_comma_separator}}₫" );
		totalPriceText = totalPriceText.match(regex)[0];
		var regInput = new RegExp(unitPriceText, "g");
		var totalPriceHtml = jQuery('.quick-view-product .price').html().replace(regInput ,totalPriceText);
		jQuery('.quick-view-product .total-price span').html(totalPriceHtml);
	}
}
$(document).on('click', '.quickview-close, #overlay, .quickview-overlay, .fancybox-overlay', function(e){
	$("#quick-view-product").fadeOut(0);
	dqdt.hidePopup('.loading');
});
/*Bắt trường hợp số âm number*/
$(document).on('click','span.btn_minus_qv',function(e){
	var va = parseInt($(this).parent().find('input').val());
	if(va > 1){
		$(this).parent().find('input').val(va - 1);
	}
});

$(document).on('click','span.btn_plus_qv',function(e){
	var va = parseInt($(this).parent().find('input').val());
	$(this).parent().find('input').val(va + 1);

});
/*Menu hover*/

/*product variant
$( ".sp-thumbnail-image" ).click(function() {
  var src= $(this).attr('data-image');
 $('.image_big').attr('src',src);
});*/
jQuery(document).ready(function (e) {
	var numInput = document.querySelector('#cart-sidebar input.input-text');
	if (numInput != null){
		// Listen for input event on numInput.
		numInput.addEventListener('input', function(){
			// Let's match only digits.
			var num = this.value.match(/^\d+$/);
			if (num === null) {
				// If we have no match, value will be empty.
				this.value = "";
			}
		}, false)
	}
});
jQuery(document).ready(function (e) {
	var numInput = document.querySelector('.bg-scroll input.input-text');
	if (numInput != null){
		// Listen for input event on numInput.
		numInput.addEventListener('input', function(){
			// Let's match only digits.
			var num = this.value.match(/^\d+$/);
			if (num === null) {
				// If we have no match, value will be empty.
				this.value = "";
			}
		}, false)
	}
});